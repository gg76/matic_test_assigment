# frozen_string_literal: true
require 'active_record'

# Stores responses of both bot and user
class Message < ActiveRecord::Base
  belongs_to :conversation

  def question?
    question
  end

  def answer?
    !question?
  end
end
