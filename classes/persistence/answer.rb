# frozen_string_literal: true
require 'active_record'

# Stores user answers in answer_name, answer_value pair
class Answer < ActiveRecord::Base
  belongs_to :user

  def self.build_from_pair(pair)
    new(answer_name: pair.first, answer_value: pair[1])
  end
end
