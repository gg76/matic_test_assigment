# frozen_string_literal: true
require 'active_record'
require_relative 'answer'
require_relative '../../classes/userdata'

# Holds conversation and answers. Returns userdata for easier access to answers
class User < ActiveRecord::Base
  has_many :answers
  has_one :conversation

  def userdata
    hash = {}
    answers.pluck(:answer_name, :answer_value).each do |pair|
      hash.merge!(pair.first => pair[1])
    end
    Userdata.new(hash)
  end
end
