# frozen_string_literal: true
require 'active_record'
require_relative 'message'

# Stores messages for logging purposes
class Conversation < ActiveRecord::Base
  belongs_to :user
  has_many :messages

  def self.build_from_messages(messages)
    array = []
    messages.each_slice(2) do |message|
      array << Message.new(text: message.first, question: true)
      array << Message.new(text: message[1], question: false)
    end
    Conversation.new(messages: array)
  end
end
