# frozen_string_literal: true
# Stores ordered questions and passes responsibly around
class Scenario
  attr_reader :scenario, :question_printer, :answer_receiver

  def initialize(question_printer, answer_receiver, hash)
    @question_printer = question_printer
    @answer_receiver = answer_receiver
    @scenario = hash
    @conversation = { values: {}, transcript: {}, conversation: [] }
  end

  def start_conversation
    ask_question(scenario[1])
  end

  private

  def answers
    @conversation[:values]
  end

  def ask_question(question)
    return @conversation unless question

    print_question_text(question)
    user_answer = receive_answer(question)
    @conversation = merge_hashes(@conversation, user_answer)

    ask_question(next_question_id(question))
  end

  def next_question_id(current_question)
    scenario[current_question.next_question_id(@conversation[:transcript])]
  end

  def print_question_text(question)
    question_printer.print_question(question, answers)
  end

  def receive_answer(question)
    answer_receiver.receive_for_question(question_printer, answers, question)
  end

  def merge_hashes(source_hash, target_hash)
    source_hash.merge(target_hash) do |_, v1, v2|
      if v1.is_a?(Hash) && v2.is_a?(Hash)
        v1.merge(v2)
      else
        v1 + v2
      end
    end
  end
end
