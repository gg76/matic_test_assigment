# frozen_string_literal: true
require_relative '../modules/hash_methods'

# Wrapper around answers
class Userdata
  include HashMethods

  def keys
    original_data.keys
  end
end
