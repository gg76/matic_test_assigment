# frozen_string_literal: true
require_relative '../../modules/hash_methods'

# Wrapper around non-specific question
class ValueQuestion
  include HashMethods

  def validate_answer(answer)
    answer
  end
end
