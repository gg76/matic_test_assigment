# frozen_string_literal: true
require_relative '../../modules/hash_methods'

# Accepts only valid numbers from choice list
class ChoiceQuestion
  include HashMethods

  def question_text
    original_data['question_text'] + prepare_choice_text
  end

  def validate_answer(answer)
    choice_text_by_number(answer) if answer =~ /\A[-+]?[0-9]+\z/
  end

  def next_question_id(transcript)
    chosen_answer = choice_by_choice_text(transcript[question_id])
    choice_next_question_id(chosen_answer)
  end

  private

  def prepare_choice_text
    prompt = "\n"

    choices.each_with_index do |choice, index|
      prompt += "#{index}. #{choice['choice_text']}\n"
    end
    prompt
  end

  def choice_text_by_number(choice_number)
    chosen_answer = choice_number.to_i
    choices[chosen_answer]['choice_text'] if choices[chosen_answer]
  end

  def choice_next_question_id(choice)
    choice['next_question_id']
  end

  def choice_by_choice_text(text)
    object = nil
    choices.each do |choice|
      object = choice if choice['choice_text'] == text
    end
    object
  end
end
