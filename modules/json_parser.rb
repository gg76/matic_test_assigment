# frozen_string_literal: true
require 'json'

# Consume filepath, return parsed json
module JsonParser
  module_function

  def parse_file(file_path)
    contents = File.read(file_path)
    JSON.parse(contents)
  end
end
