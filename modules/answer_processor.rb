# frozen_string_literal: true
# Creates Answers from hash passing responsibility down the ladder
module AnswerProcessor
  module_function

  def process_answers(answer_class, answers_hash)
    array = []
    answers_hash.each do |pair|
      array << answer_class.build_from_pair(pair)
    end
    array
  end
end
