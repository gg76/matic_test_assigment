# frozen_string_literal: true
Dir[File.join(__dir__, '../classes/questions/*.rb')].sort.each do |file|
  require file
end

# Pass responsibility of defining behaviour to respective classes
module QuestionBuilder
  module_function

  def prepare_question(hash)
    question_class_name(hash).new(hash)
  end

  def question_class_name(hash)
    Kernel.const_get(hash['question_type'].capitalize + 'Question')
  end
end
