# frozen_string_literal: true
# Same as QuestionBuilder but for all questions at once
module ScenarioBuilder
  module_function

  def prepare_step(question_builder, hash)
    { hash['question_id'] => question_builder.prepare_question(hash) }
  end

  def build_scenario(question_builder, array)
    result = {}
    array.each do |hash|
      result.merge! prepare_step(question_builder, hash)
    end
    result
  end
end
