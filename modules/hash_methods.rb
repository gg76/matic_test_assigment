# frozen_string_literal: true
# Our all-in-one data store (famous NullObject). Note, that I don't fallback on super in method_missing. This is design choice.
module HashMethods
  def initialize(hash)
    @hash = hash
  end

  def original_data
    @hash
  end

  def method_missing(method_name, *_arguments)
    value = original_data[method_name.to_s]
    return value if value
    nil
  end

  def respond_to_missing?(method_name, include_private = false)
    original_data[method_name.to_s] || super
  end
end
