# frozen_string_literal: true
# Takes in question and asks until it receives valid answer. Validness is defined by question itself.
module AnswerReceiver
  module_function

  def pretty_please_prompt
    'Could you please try again?'
  end

  def receive_for_question(text_printer, answers, question)
    loop_conversation = asking_loop(question)

    loop_conversation.unshift(text_printer.transform_question(question.question_text, answers))

    prepare_response(question, loop_conversation, loop_conversation.last)
  end

  def asking_loop(question)
    loop_conversation = []
    answer = nil

    loop do
      local_answer = gets.chomp
      answer = question.validate_answer(local_answer)
      break if answer
      loop_conversation += [local_answer, pretty_please_prompt]
      puts pretty_please_prompt
    end

    loop_conversation << answer
  end

  def prepare_response(question, loop_conversation, answer)
    response = {}

    response[:transcript] = { question.question_id => answer }
    response[:conversation] = loop_conversation
    response[:values] = { question.value_name => answer } if question.value_name

    response
  end
end
