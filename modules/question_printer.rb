# frozen_string_literal: true
# Prints out question replacing placeholders with real values, if they exist
module QuestionPrinter
  module_function

  MATCHER = /:\((\D*?)\)/
  UTILITY = /:\(|\)/

  def transform_question(question_text, values)
    question_text.gsub(MATCHER) do |match|
      resolved = values[strip_utility(match)]
      (match && resolved) || match
    end
  end

  def print_question(question, values)
    puts transform_question(question.question_text, values)
  end

  def strip_utility(string)
    string.gsub(UTILITY, '')
  end
end
