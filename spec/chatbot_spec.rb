# frozen_string_literal: true
files_to_load = Dir["#{__dir__}/../modules/*.rb"]
files_to_load += Dir["#{__dir__}/../classes/questions/*.rb"]
files_to_load += Dir["#{__dir__}/../classes/persistence/*.rb"]
files_to_load += Dir["#{__dir__}/../classes/*.rb"]
# files_to_load += Dir["#{__dir__}/../config/*.rb"]

files_to_load.sort.each { |file| require file }

describe 'chatbot' do
  let(:answers) { { intro: '0', name: 'Andriy', contact_choice: '1', email_address: 'erp.lsf@gmail.com', correct_choice: '0' } }
  subject(:user) do
    parsed_file = JsonParser.parse_file("#{__dir__}/../scenarios/test_scenario.json")
    scenario = ScenarioBuilder.build_scenario(QuestionBuilder, parsed_file)
    question_printer = QuestionPrinter
    answer_receiver = AnswerReceiver
    scenario = Scenario.new(question_printer, answer_receiver, scenario)

    allow(question_printer).to receive(:puts)
    allow(answer_receiver).to receive(:gets).and_return(*answers.values)

    conversation = scenario.start_conversation

    user = User.new
    user.answers << AnswerProcessor.process_answers(Answer, conversation[:values])
    user.conversation = Conversation.build_from_messages(conversation[:conversation])
    user.save!
    user
  end

  describe 'system produces correct results' do
    let(:first_question_text) { "I can help you with answers on all your related questions and help to find a great job!\n0. Let's talk\n" }
    let(:last_user_answer) { 'Yes, please' }

    it { expect(subject.userdata.intro).to eq('Let\'s talk') }
    it { expect(subject.userdata.name).to eq(answers[:name]) }
    it { expect(subject.userdata.contact_choice).to eq('Email') }
    it { expect(subject.userdata.email_address).to eq(answers[:email_address]) }

    it { expect(subject.conversation.messages.first.text).to eq(first_question_text) }
    it { expect(subject.conversation.messages.last.text).to eq(last_user_answer) }
  end
end
