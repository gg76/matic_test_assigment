# frozen_string_literal: true
require_relative '../../classes/scenario'
require_relative '../../modules/question_builder'
require_relative '../../modules/scenario_builder'

describe Scenario do
  let(:json) { { 'question_id' => 1, 'question_text' => 'Sample Text', 'question_type' => 'value', 'next_question_id' => 2 } }
  let(:another_json) { { 'question_id' => 2, 'question_text' => 'Another Text', 'question_type' => 'value' } }
  let(:builder) { QuestionBuilder }
  let(:input) { ScenarioBuilder.build_scenario(builder, [json, another_json]) }
  let(:printer_stub) { double }
  let(:receiver_stub) { double }
  let(:first_question) { subject.scenario[1] }
  let(:second_question) { subject.scenario[2] }

  subject { Scenario.new(printer_stub, receiver_stub, input) }

  describe '#start_conversation' do
    it 'starts with first question' do
      expect(subject).to receive(:ask_question).with(first_question)

      subject.start_conversation
    end
  end

  describe '#ask_question' do
    it 'passes printing and receiving to another member' do
      allow(printer_stub).to receive(:print_question)
      allow(receiver_stub).to receive(:receive_for_question) { {} }

      expect(printer_stub).to receive(:print_question)
      expect(receiver_stub).to receive(:receive_for_question)

      subject.send(:ask_question, second_question)
    end

    it 'asks questions until there none' do
      allow(receiver_stub).to receive(:receive_for_question) { {} }
      allow(printer_stub).to receive(:transform_question) { {} }

      expect(printer_stub).to receive(:print_question).with(first_question, {})
      expect(printer_stub).to receive(:print_question).with(second_question, {})

      expect(receiver_stub).to receive(:receive_for_question).with(printer_stub, {}, first_question)
      expect(receiver_stub).to receive(:receive_for_question).with(printer_stub, {}, second_question)

      subject.send(:ask_question, first_question)
    end
  end
end
