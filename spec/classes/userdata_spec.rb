# frozen_string_literal: true
require_relative '../../classes/userdata'

describe Userdata do
  let(:input) { { 'name' => 'value' } }
  subject { Userdata.new(input) }

  describe '#keys' do
    it { expect(subject.keys).to eq(input.keys) }
  end
end
