# frozen_string_literal: true
require_relative '../../../classes/persistence/conversation'
require_relative '../../../config/db_connection'

describe Conversation do
  describe '.build_from_messages' do
    let(:input) { ['Bot question', 'User answer', 'Question again', 'Another answer'] }
    subject { Conversation.build_from_messages(input) }

    it { expect(subject.messages.first.text).to eq(input.first) }
    it { expect(subject.messages.first.question).to eq(true) }

    it { expect(subject.messages[1].text).to eq(input[1]) }
    it { expect(subject.messages[1].question).to eq(false) }

    it { expect(subject.messages[2].text).to eq(input[2]) }
    it { expect(subject.messages[2].question).to eq(true) }
  end
end
