# frozen_string_literal: true
require_relative '../../../classes/persistence/message'
require_relative '../../../config/db_connection'

describe Message do
  let(:input) { { text: 'Some text', question: true } }
  subject { described_class.new(input) }

  describe '#question?' do
    it { expect(subject.question?).to eq(true) }
  end

  describe '#answer?' do
    it { expect(subject.answer?).to eq(false) }
  end
end
