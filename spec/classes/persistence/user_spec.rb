# frozen_string_literal: true
require_relative '../../../classes/persistence/user'
require_relative '../../../classes/persistence/answer'
require_relative '../../../modules/answer_processor'
require_relative '../../../config/db_connection'

describe User do
  let(:values) { { 'username' => 'Monkey', 'dad' => 'mom', 'stuff' => 'another' } }
  let(:processed_answers) { AnswerProcessor.process_answers(Answer, values) }

  let(:user) do
    user = User.new
    user.answers << AnswerProcessor.process_answers(Answer, values)
    user.save!
    user
  end

  describe '#userdata' do
    it { expect(user.userdata.keys).to eq(values.keys) }
    it { expect(user.userdata.username).to eq(values['username']) }
    it { expect(user.userdata.dad).to eq(values['dad']) }
    it { expect(user.userdata.stuff).to eq(values['stuff']) }
    it { expect(user.userdata.nonexistent_answer).to eq(nil) }
  end
end
