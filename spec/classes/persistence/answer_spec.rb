# frozen_string_literal: true
require_relative '../../../classes/persistence/answer'
require_relative '../../../config/db_connection'

describe Answer do
  let(:input) { %w(username Monkey) }
  subject { Answer.build_from_pair(input) }

  describe '.build_from_pair' do
    it { expect(subject.answer_name).to eq(input.first) }
    it { expect(subject.answer_value).to eq(input[1]) }
  end
end
