# frozen_string_literal: true
require_relative '../../../classes/questions/choice_question'
require_relative '../../../modules/question_builder'

describe ChoiceQuestion do
  let!(:input) do
    {
      'question_id' => 7,
      'question_text' => 'What is the best time we can reach out to you?',
      'question_type' => 'choice',
      'value_name' => 'reach_out_time',
      'choices' => [
        {
          'choice_text' => 'ASAP',
          'next_question_id' => 5
        },
        {
          'choice_text' => 'Morning',
          'next_question_id' => 6
        },
        {
          'choice_text' => 'Afternoon',
          'next_question_id' => 7
        },
        {
          'choice_text' => 'Evening',
          'next_question_id' => 8
        }
      ]
    }
  end
  let(:transcript) { { 7 => 'Evening' } }

  subject { ChoiceQuestion.new(input) }

  it { expect(subject.question_id).to eq(7) }
  it { expect(subject.next_question_id(transcript)).to eq(8) }
  it { expect(subject.validate_answer('2')).to eq('Afternoon') }
  it { expect(subject.validate_answer('4')).to eq(nil) }
  it { expect(subject.question_text).to eq("What is the best time we can reach out to you?\n0. ASAP\n1. Morning\n2. Afternoon\n3. Evening\n") }
end
