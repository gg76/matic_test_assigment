# frozen_string_literal: true
require_relative '../../modules/answer_processor'

describe AnswerProcessor do
  describe '.process_answers' do
    let(:values) { { 'username' => 'Monkey' } }
    let(:standard) { double }
    subject { AnswerProcessor.process_answers(Answer, values) }

    it { is_expected.to be_kind_of(Array) }

    it do
      allow(standard).to receive(:answer_name) { 'username' }
      allow(standard).to receive(:answer_value) { 'Monkey' }

      expect(subject.first.answer_name).to eq(standard.answer_name)
      expect(subject.first.answer_value).to eq(standard.answer_value)
    end
  end
end
