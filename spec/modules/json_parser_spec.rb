# frozen_string_literal: true
require_relative '../../modules/json_parser'

describe JsonParser do
  context '.parse_file' do
    let!(:file_path) { 'spec/scenarios/one.json' }
    let(:result) { JsonParser.parse_file(file_path) }
    let(:standard) { [{ 'question_id' => 1, 'question_text' => 'Sample Text', 'question_type' => 'automatic' }] }

    it { expect(result).to eq(standard) }
  end
end
