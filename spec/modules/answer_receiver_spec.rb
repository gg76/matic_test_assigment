# frozen_string_literal: true
require_relative '../../modules/answer_receiver'

describe AnswerReceiver do
  describe '.receive_for_question' do
    let(:standard) { { values: { 'username' => 'Monkey' }, transcript: { 'Sample Question Text' => 'Monkey' } } }
    let(:question) { double }
    let(:printer_stub) { double }
    let(:answer) { 'Monkey' }

    it 'returns correct values with valid input' do
      allow(subject).to receive(:gets) { answer }
      allow(subject).to receive(:receive_for_question) { { transcript: { question.question_text => answer }, values: { question.value_name => answer } } }

      allow(question).to receive(:value_name) { 'username' }
      allow(question).to receive(:question_text) { 'Sample Question Text' }

      expect(subject).to receive(:receive_for_question)
      expect(subject.receive_for_question(printer_stub, {}, question)).to eq(standard)
    end

    let(:answers) { { first_answer: '1984', mocking_answer: 'you are a moron', correct_answer: '1' } }

    it 'asks until valid answer is provided' do
      allow(printer_stub).to receive(:puts)
      allow(printer_stub).to receive(:transform_question) {}

      allow(subject).to receive(:gets).and_return(*answers.values)
      allow(subject).to receive(:puts)

      allow(question).to receive(:question_id) { '1' }
      allow(question).to receive(:question_text) { 'Sample Question Text' }
      allow(question).to receive(:value_name) { '' }
      allow(question).to receive(:validate_answer) { |value| value == '1' ? value : false }

      expect(subject).to receive(:gets).exactly(3).times

      subject.receive_for_question(printer_stub, {}, question)
    end
  end
end
