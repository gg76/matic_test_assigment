# frozen_string_literal: true
require_relative '../../modules/scenario_builder.rb'
require_relative '../../classes/questions/value_question'
require_relative '../../modules/question_builder'

describe ScenarioBuilder do
  let(:input) { { 'question_id' => 1, 'question_text' => 'Sample Text', 'question_type' => 'value' } }
  let(:another_input) { { 'question_id' => 2, 'question_text' => 'Another Text', 'question_type' => 'value' } }
  let(:automatic_question) { ValueQuestion.new(input) }
  let(:another_automatic_question) { ValueQuestion.new(another_input) }
  let(:builder) { QuestionBuilder }

  context '.prepare_step' do
    let(:result) { subject.prepare_step(builder, input) }
    let(:standard) { { 1 => automatic_question } }

    it { expect(result[1].original_data).to eq(standard[1].original_data) }
  end

  context '.build_scenario' do
    let(:result) { subject.build_scenario(builder, [input, another_input]) }
    let(:standard) { { 1 => automatic_question, 2 => another_automatic_question } }

    it { expect(result[1].original_data).to eq(standard[1].original_data) }
    it { expect(result[2].original_data).to eq(standard[2].original_data) }
  end
end
