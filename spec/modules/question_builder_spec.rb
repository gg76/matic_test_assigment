# frozen_string_literal: true
require_relative '../../modules/question_builder'

describe QuestionBuilder do
  let(:input) { { 'question_id' => 1, 'question_text' => 'Sample Text', 'question_type' => 'value' } }
  let(:invalid_input) { { 'question_id' => 1, 'question_text' => 'Sample Text', 'question_type' => 'invalid' } }

  context '.prepare_class_name' do
    let(:input_choice) { { 'question_id' => 1, 'question_text' => 'Sample Text', 'question_type' => 'choice' } }
    let(:input_value) { { 'question_id' => 1, 'question_text' => 'Sample Text', 'question_type' => 'value' } }

    context 'with valid input' do
      it { expect(subject.question_class_name(input_choice)).to eq(ChoiceQuestion) }
      it { expect(subject.question_class_name(input_value)).to eq(ValueQuestion) }
    end

    context 'with invalid input' do
      it { expect { subject.question_class_name(invalid_input) }.to raise_error(NameError, 'uninitialized constant Kernel::InvalidQuestion') }
    end
  end

  context '.prepare_question' do
    context 'with valid input' do
      subject { QuestionBuilder.prepare_question(input) }

      it { expect(subject.question_id).to eq(1) }
      it { expect(subject.question_text).to eq('Sample Text') }
      it { expect(subject.question_type).to eq('value') }
      it { expect(subject.next_question_id).to eq(nil) }
    end

    context 'with invalid input' do
      subject { QuestionBuilder.prepare_question(invalid_input) }

      it { expect { subject.question_class_name(invalid_input) }.to raise_error(NameError, 'uninitialized constant Kernel::InvalidQuestion') }
    end
  end
end
