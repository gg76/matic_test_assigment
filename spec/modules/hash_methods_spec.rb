# frozen_string_literal: true
require_relative '../../modules/hash_methods'

describe HashMethods do
  let(:input) { { 'question_id' => 1, 'question_text' => 'Sample Text', 'question_type' => 'automatic', 'next_question_id' => 2 } }
  subject { (Class.new { include HashMethods }).new(input) }

  it { expect(subject.question_id).to eq(1) }
  it { expect(subject.question_text).to eq('Sample Text') }
  it { expect(subject.question_type).to eq('automatic') }
  it { expect(subject.next_question_id).to eq(2) }
  it { expect(subject.original_data).to eq(input) }
  it { expect(subject.nonexistent_value).to eq(nil) }
end
