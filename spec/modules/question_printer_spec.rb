# frozen_string_literal: true
require_relative '../../modules/question_printer'

describe QuestionPrinter do
  describe '.print_question' do
    let(:question) { double }

    context 'with one one value' do
      let(:question_text) { 'Hello, :(username)!' }
      let(:values) { { 'username' => 'Monkey' } }

      it do
        allow(question).to receive(:question_text) { question_text }
        expect { subject.print_question(question, values) }.to output("Hello, #{values['username']}!\n").to_stdout
      end
    end

    context 'with multiple values' do
      let(:question_text) { 'Hello, :(username)! How is your :(relative)?' }
      let(:values) { { 'username' => 'Monkey', 'relative' => 'sister' } }

      it do
        allow(question).to receive(:question_text) { question_text }
        expect { subject.print_question(question, values) }.to output("Hello, #{values['username']}! How is your #{values['relative']}?\n").to_stdout
      end
    end

    context 'with some values' do
      let(:question_text) { 'Hello, :(username)! How is your :(relative)?' }
      let(:values) { { 'username' => 'Monkey' } }

      it do
        allow(question).to receive(:question_text) { question_text }
        expect { subject.print_question(question, values) }.to output("Hello, #{values['username']}! How is your :(relative)?\n").to_stdout
      end
    end

    context 'with excess parentheses' do
      let(:question_text) { 'Hello, :(username))))!' }
      let(:values) { { 'username' => 'Monkey' } }

      it do
        allow(question).to receive(:question_text) { question_text }
        expect { subject.print_question(question, values) }.to output("Hello, #{values['username']})))!\n").to_stdout
      end
    end
  end

  context '.strip_utility' do
    let(:question_text) { 'Hello, :(username)!' }

    it { expect(subject.strip_utility(question_text)).to eq('Hello, username!') }
  end
end
