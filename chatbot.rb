# frozen_string_literal: true
require 'pp'
require 'bundler'

Bundler.require

files_to_load = Dir['./modules/*.rb']
files_to_load += Dir['./classes/questions/*.rb']
files_to_load += Dir['./classes/persistence/*.rb']
files_to_load += Dir['./classes/*.rb']
files_to_load += Dir['./config/*.rb']
files_to_load.sort.each { |file| require file }

parsed_file = JsonParser.parse_file('./scenarios/test_scenario.json')
scenario = ScenarioBuilder.build_scenario(QuestionBuilder, parsed_file)
question_printer = QuestionPrinter
answer_receiver = AnswerReceiver
scenario = Scenario.new(question_printer, answer_receiver, scenario)

conversation = scenario.start_conversation

user = User.new
user.answers << AnswerProcessor.process_answers(Answer, conversation[:values])
user.conversation = Conversation.build_from_messages(conversation[:conversation])
user.save!
