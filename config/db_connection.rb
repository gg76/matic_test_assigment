# frozen_string_literal: true
require 'bundler'
require 'fileutils'

Bundler.require(:db)

ROOT_PATH = "#{__dir__}/.."
DB_ENV ||= 'development'
DB_PATH = "#{ROOT_PATH}/db/chatbot_#{DB_ENV || 'development'}"
LOG_PATH = "#{ROOT_PATH}/log/database_#{DB_ENV}.log"

dirname = File.dirname(LOG_PATH)
FileUtils.mkdir_p(dirname) unless File.directory?(dirname)

ActiveRecord::Base.logger = Logger.new(File.new(LOG_PATH, 'w'))

ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: DB_PATH)

ActiveRecord::Schema.define do |schema|
  schema.verbose = false

  unless ActiveRecord::Base.connection.data_sources.include? 'users'
    create_table :users, &:timestamps
  end

  unless ActiveRecord::Base.connection.data_sources.include? 'answers'
    create_table :answers do |table|
      table.column :user_id, :integer
      table.column :answer_name, :string
      table.column :answer_value, :string
      table.timestamps
    end
  end

  unless ActiveRecord::Base.connection.data_sources.include? 'conversations'
    create_table :conversations do |table|
      table.column :user_id, :integer
      table.timestamps
    end
  end

  unless ActiveRecord::Base.connection.data_sources.include? 'messages'
    create_table :messages do |table|
      table.column :conversation_id, :integer
      table.column :question, :boolean
      table.column :text, :string
      table.timestamps
    end
  end
end
